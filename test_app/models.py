from django.db import models

# Create your models here.


class ObjectWithFields(models.Model):
    name = models.TextField(max_length=100)
    content = models.TextField(max_length=100)

    def __str__(self):
        return self.name + "|" + self.content

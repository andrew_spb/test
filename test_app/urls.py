from django.conf.urls import url
import test_app
from test_app import views

urlpatterns = [

    url(r'^$',test_app.views.show_objects, name='index'),
    url(r'^(?P<object_id>[0-9]+)/$', views.show_current_object, name='show'),

]

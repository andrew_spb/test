from django.shortcuts import render, get_object_or_404
from test_app.models import ObjectWithFields

# Create your views here.


def show_objects(request):
    my_object = ObjectWithFields.objects.all()
    context = {
        'my_object': my_object
    }
    return render(request, 'test_app/index.html', context)


def show_current_object(request, object_id):
    my_object = get_object_or_404(ObjectWithFields, pk=object_id)
    return render(request, 'test_app/show.html', {'my_object': my_object})